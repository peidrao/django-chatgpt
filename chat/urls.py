
from django.urls import path
from chat.views import HomeView, ChatDetailView


urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('chat/<int:id>', ChatDetailView.as_view(), name='chat_detail')
]
