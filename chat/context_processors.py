from chat.models import Chat


def chats(request):
    chats = Chat.objects.all().order_by('-created_at')
    return {'chats': chats}