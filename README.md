# UI Clone ChatGPT - Django

## Como usar o projeto?

```bash
# clone o repositorio
$ git clone https://gitlab.com/peidrao/django-chatgpt.git
# entre no diretorio
$ cd django-chatgpt
# Instalando dependencias
$ pip install -r requirements.txt
# Criando migracoes no banco de dados
$ python manage.py migrate
# Fazer o start do projeto.
$ python manage.py runserver
```

## Gerar API KEY pelo OpenAI

E importante acessar esse [link](https://platform.openai.com/overview)

**Personal -> View API keys -> Create new secret key**

